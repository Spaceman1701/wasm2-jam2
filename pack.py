# Python script to package all obj files in "res"
# as C files

import os
import math


class MeshIndex:
    def __init__(self, vertex, normal, uv):
        self.vertex = vertex
        self.normal = normal
        self.uv = uv

    def parse(data: str):
        parts = data.strip().split("/")

        vert = int(parts[0]) - 1
        norm = None
        uv = None
        if parts[2] != "":
            norm = int(parts[2]) - 1
        if parts[1] != "":
            uv = int(parts[1]) - 1
        return MeshIndex(vert, norm, uv)

    def to_int(self):
        uv = (self.uv & 0b1111111111) << 20
        norm = (self.normal & 0b1111111111) << 10
        vertex = self.vertex & 0b1111111111
        return uv | norm | vertex


class Vec3:
    def __init__(self, x, y, z):
        self.x = x
        self.y = y
        self.z = z

    def parse(data: str):
        components = data.strip().split(" ")
        if len(components) != 3:
            raise Exception("expected Vec3")

        return Vec3(float(components[0]), float(components[1]), float(components[2]))

    def to_c(self):
        return "{" + str(self.x) + "f, " + str(self.y) + "f, " + str(self.z) + "f}"

    def add(self, other):
        return Vec3(self.x + other.x, self.y + other.y, self.z + other.z)


class Vec2:
    def __init__(self, u, v):
        self.u = u
        self.v = v

    def parse(data: str):
        components = data.strip().split(" ")
        if len(components) != 2:
            raise Exception("expected Vec2")

        return Vec2(float(components[0]), float(components[1]))

    def to_c(self):
        return "{" + str(self.u) + "f, " + str(self.v) + "f}"


class CollisionSphere:
    def __init__(self, verticies):
        self.verticies = verticies
        self.center = None
        self.radius = None

    def compute_values(self):
        sum = Vec3(0, 0, 0)
        for v in self.verticies:
            sum = sum.add(v)

        sum.x /= len(self.verticies)
        sum.y /= len(self.verticies)
        sum.z /= len(self.verticies)

        self.center = sum
        self.radius = 0
        for v in self.verticies:
            diff = Vec3(v.x - self.center.x, v.y -
                        self.center.y, v.z - self.center.z)
            dist = math.sqrt(diff.x*diff.x + diff.y*diff.y + diff.z*diff.z)
            if dist > self.radius:
                self.radius = dist


class Mesh:
    def __init__(self, filename, collision_file):
        self.filename = filename
        self.name = os.path.basename(self.filename)
        self.simple_name = self.name.split(".")[0]

        self.collsion_mesh_file = collision_file
        self.colliders = []

        self.verticies = []
        self.normals = []
        self.uvs = []

        self.indicies = []

    def load(self):
        with open(self.filename) as filedata:
            lines = filedata.readlines()
        i = 0

        try:
            for line in lines:
                if line.startswith("v "):
                    self.verticies.append(Vec3.parse(line[2:]))
                if line.startswith("vt "):
                    self.uvs.append(Vec2.parse(line[3:]))
                if line.startswith("vn "):
                    self.normals.append(Vec3.parse(line[3:]))
                if line.startswith("f "):
                    face = line[2:].strip()
                    face_indicies = face.split(" ")
                    if len(face_indicies) != 3:
                        raise Exception(f"only triangles are supported")
                    for index in face_indicies:
                        self.indicies.append(MeshIndex.parse(index))
                i += 1
        except Exception as e:
            raise Exception(f"parse error at {self.filename}:{i}") from e
        print(self.pretty_print())

        if self.load_collision():
            print("loaded collision data")

    def load_collision(self):
        if self.collsion_mesh_file is None:
            return False
        try:
            print("opening file:", self.collsion_mesh_file)
            with open(self.collsion_mesh_file) as filedata:
                lines = filedata.readlines()
        except:
            return False

        cur_collider = None
        for line in lines:
            if line.startswith("o "):
                if not cur_collider is None:
                    self.colliders.append(cur_collider)
                cur_collider = CollisionSphere([])
            if line.startswith("v "):
                cur_collider.verticies.append(Vec3.parse(line[2:]))
        if not cur_collider is None:
            self.colliders.append(cur_collider)
        for c in self.colliders:
            c.compute_values()
            print(f"collider: [r={c.radius}]")
        return True

    def pretty_print(self) -> str:
        return f"Mesh {self.name} ({int(len(self.indicies)/3)} triangles) (appox. {self.estimate_memory()} bytes):\n  {len(self.verticies)} verticies\n  {len(self.normals)} normals\n  {len(self.uvs)} uvs\n "

    def estimate_memory(self):
        vertex_mem = len(self.verticies)*4*3
        norm_mem = len(self.normals)*4*3
        uv_mem = len(self.normals)*4*2
        index_mem = len(self.indicies)*4
        return vertex_mem + norm_mem + uv_mem + index_mem

    def ctor_name(self):
        return f"mesh_{self.name.split('.')[0]}_new"

    def make_header_snippet(self):
        return f"Mesh {self.ctor_name()}();"

    def make_collision_header_snippet(self):
        return f"CollisionMesh {self.ctor_name()}_collision();"

    def make_collider_data_consts(self):
        centers = "static vec3 centers[] = {"
        radius = "static float radius[] = {"

        for c in self.colliders:
            centers += f"{c.center.to_c()},"
            radius += f"{c.radius},"

        centers += "};"
        radius += "};"
        return f"{centers}\n{radius}\n"

    def make_data_consts(self):
        verts = "static vec3 verticies[] = {"
        norms = "static vec3 normals[] = {"
        uvs = "static vec2 uvs[] = {"
        indicies = "static MeshIndex indicies[] = {"

        for v in self.verticies:
            verts += f"{v.to_c()},"
        for n in self.normals:
            norms += f"{n.to_c()},"
        for u in self.uvs:
            uvs += f"{u.to_c()},"
        for i in self.indicies:
            indicies += f"{i.to_int()},"

        verts += "};"
        norms += "};"
        uvs += "};"
        indicies += "};"
        return f"{verts}\n{norms}\n{uvs}\n{indicies}\n"

    def make_c_impl(self):
        includes = '#include "resources.h"\n#include "mesh.h"\n#include "cglm/types.h"\n'
        consts = self.make_data_consts() + "\n\n"

        assigns = f".verticies=verticies,.normals=normals,.uvs=uvs,.indicies=indicies,.indicies_len={len(self.indicies)}"
        func = f"Mesh {self.ctor_name()}() " + \
            "{" + "\nreturn (Mesh) {" + assigns + "};}\n\n"

        if len(self.colliders) > 0:
            consts += self.make_collider_data_consts()
            col_assigns = f".center=centers, .radius=radius, .count={len(self.colliders)}"
            col_func = f"CollisionMesh {self.ctor_name()}_collision()" + \
                "{" + "\nreturn (CollisionMesh) {" + col_assigns + "};}"
            func += col_func

        return includes + consts + func


if __name__ == "__main__":
    try:
        os.makedirs("build/res")
    except:
        pass
    header_funcs = '#include "mesh.h"\n\n'
    for file in os.listdir("res"):
        if not file.endswith(".obj"):
            continue
        if file.endswith(".collision.obj"):
            continue

        real_file = os.path.join("res", file)
        collision_file = None
        shortname = os.path.basename(real_file).split(".")[0]
        if os.path.exists(os.path.join("res", shortname + ".collision.obj2")):
            collision_file = os.path.join("res", shortname + ".collision.obj2")
        m = Mesh(real_file, collision_file)
        m.load()

        header_funcs += m.make_header_snippet()
        if len(m.colliders) > 0:
            header_funcs += m.make_collision_header_snippet()
        outimpl = m.make_c_impl() + "\n\n"
        with open(f"build/res/{m.name}.c", "w") as outfile:
            outfile.write(outimpl)
    with open("build/res/resources.h", "w") as resource_header:
        resource_header.write(header_funcs)
