# Super WASM Star Pilot

WASM4 game jam 2 game.

# Building

You need a modern Python installation, WASI 14.0, and make.

WASM4 is also needed for running.

# Flavor

In _Star Pilot_ play as an elite star fighter pilot to destroy enemy fighters, aseroids, bases.

Experience full rasterized 3d graphics with realistic 2 color lighting. 

Use the D-pad to stear your fighter and avoid obsticals while using the A-button to fire torpedos and score points!

Be careful, if you take too much damange your fighter will be destroyed!

# Attribution

Space Ship Model: https://opengameart.org/content/star-ship-low-poly
provided under CC by IllusionOfMana

