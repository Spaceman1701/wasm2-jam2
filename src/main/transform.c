#include "transform.h"
#include "cglm/affine.h"
#include "cglm/mat3.h"
#include "cglm/mat4.h"
#include "cglm/quat.h"
#include "cglm/vec3.h"

void transform_init(Transform *t) {
  *t = (Transform){
      .pos = GLM_VEC3_ZERO_INIT,
      .rot = GLM_QUAT_IDENTITY_INIT,
      .scale = GLM_VEC3_ONE_INIT,
  };
}
void transform_to_mat4(Transform *t, mat4 out) {

  glm_translate(out, t->pos);
  glm_quat_rotate(out, t->rot, out);
  glm_scale(out, t->scale);
}

void transform_translate(Transform *t, vec3 translation) {
  glm_vec3_add(t->pos, translation, t->pos);
}

void transform_rotate(Transform *t, versor rotation) {
  glm_quat_mul(t->rot, rotation, t->rot);
  glm_quat_normalize(t->rot);
}

void transform_get_forward_vec(Transform *t, vec3 out) {
  mat4 mat = GLM_MAT4_IDENTITY_INIT;
  transform_to_mat4(t, mat);

  glm_vec3(mat[2], out);
  glm_normalize(out);
}

void transform_get_up_vec(Transform *t, vec3 out) {
  mat4 mat = GLM_MAT4_IDENTITY_INIT;
  transform_to_mat4(t, mat);

  vec3 up = {mat[1][0], mat[1][1], mat[1][2]};

  glm_vec3_copy(up, out);
  glm_normalize(out);
}

void transform_get_right_vec(Transform *t, vec3 out) {
  mat4 mat = GLM_MAT4_IDENTITY_INIT;
  transform_to_mat4(t, mat);

  mat3 rot;
  glm_mat4_pick3(mat, rot);
  glm_mat3_transpose(rot);

  glm_mat3_inv(rot, rot);

  vec3 up = {rot[0][0], rot[0][1], rot[0][2]};

  glm_vec3_copy(up, out);
  glm_normalize(out);
}

void compute_render_matricies(mat4 model, mat4 view, mat4 projection,
                              RenderMatricies *out) {
  // model view matrix
  glm_mat4_mul(view, model, out->mv);

  // model view projection
  mat4 view_proj;
  glm_mat4_mul(projection, view, view_proj);
  glm_mat4_mul(view_proj, model, out->mvp);

  // normal matrix
  mat3 tmp;
  glm_mat4_pick3(out->mv, tmp);
  glm_mat3_inv(tmp, tmp);
  glm_mat3_transpose_to(tmp, out->norm);
}