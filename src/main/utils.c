#include "utils.h"
#include "cglm/vec3.h"
#include "prelude.h"
#include <stdlib.h>
float frand() { return ((float)rand() / (float)RAND_MAX); }

void vec3_muls(vec3 v, float s, vec3 dest) {
  glm_vec3_mul(v, (vec3){s, s, s}, dest);
}

void vec3_rand(vec3 v) {
  v[0] = frand();
  v[1] = frand();
  v[2] = frand();
}

void number_to_text(i32 value, char *out, u32 len) {
  for (u32 i = 0; i < len; i++) {
    u8 digit = (u8)(value % 10);
    value /= 10;

    out[len - i - 1] = '0' + (char)digit;
  }
}
