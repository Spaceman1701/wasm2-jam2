#include "gui.h"
#include "wasm4.h"

void draw_status_bar(GuiText label, i32 x, i32 y, u32 bar_length,
                     float bar_frac, u8 bar_color) {
  u8 prev_color = *DRAW_COLORS;
  *DRAW_COLORS = bar_color;

  i32 bar_start = label.len * 8 + 1;
  i32 bar_height = 8;
  text(label.str, x, y);
  hline(bar_start, y - 1, bar_length);
  hline(bar_start, y - 1 + bar_height, bar_length + 1);
  vline(bar_start, y - 1, 8);
  vline((i32)(bar_start + (i32)bar_length), y - 1, 8);

  u32 filled_px = (u32)(bar_frac * (float)bar_length - 3);
  rect(bar_start + 2, y - 1 + 2, filled_px, 5);

  *DRAW_COLORS = prev_color;
}

void draw_bar(i32 x, i32 y, u32 bar_length, float bar_frac, u8 bar_color) {
  u8 prev_color = *DRAW_COLORS;
  *DRAW_COLORS = bar_color;

  i32 bar_start = x;
  i32 bar_height = 8;
  hline(bar_start, y - 1, bar_length);
  hline(bar_start, y - 1 + bar_height, bar_length + 1);
  vline(bar_start, y - 1, 8);
  vline((i32)(bar_start + (i32)bar_length), y - 1, 8);

  u32 filled_px = (u32)(bar_frac * (float)bar_length - 3);
  rect(bar_start + 2, y - 1 + 2, filled_px, 5);

  *DRAW_COLORS = prev_color;
}