#include "render.h"
#include "cglm/cglm.h"
#include "cglm/mat4.h"
#include "cglm/vec2.h"
#include "cglm/vec3.h"
#include "cglm/vec4.h"
#include "draw.h"
#include "mesh.h"
#include "prelude.h"
#include "screen.h"
#include "transform.h"
#include "wasm4.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef u8 udepth;
const udepth DEPTH_CLEAR_VALUE = 255;
static udepth depth_buffer[RENDER_HEIGHT * RENDER_WIDTH];

vec3 sun_direction = (vec3){-1, -1, 0.5};

udepth get_depth(i32 x, i32 y) { return depth_buffer[y * RENDER_WIDTH + x]; }

// Depth should be 0->1
udepth quantize_depth(float d) {
  if (d < 0) {
    return 0;
  }
  if (d > 1) {
    return 255;
  }
  return d * 255;
}

bool does_depth_pass(float d, i32 x, i32 y) {
  udepth real_depth = quantize_depth(d);
  udepth current_value = depth_buffer[y * RENDER_WIDTH + x];
  return real_depth < current_value;
}

void write_depth(float d, i32 x, i32 y) {
  depth_buffer[y * RENDER_WIDTH + x] = quantize_depth(d);
}

int imin(i32 a, i32 b) {
  if (a < b) {
    return a;
  } else {
    return b;
  }
}

int imax(i32 a, i32 b) {
  if (a > b) {
    return a;
  } else {
    return b;
  }
}

int imin3(i32 a, i32 b, i32 c) { return imin(a, imin(b, c)); }
int imax3(i32 a, i32 b, i32 c) { return imax(a, imax(b, c)); }

void clip_to_screen(vec3 clip_vec, ivec2 out) {
  int xscale = RENDER_WIDTH / 2;
  int yscale = RENDER_HEIGHT / 2;
  out[0] = (int)((clip_vec[0] + 1.0f) * (float)xscale);
  out[1] = (int)((clip_vec[1] + 1.0f) * (float)yscale);
}

i32 edge_func(ivec2 a, ivec2 b, ivec2 p) {
  return (b[0] - a[0]) * (p[1] - a[1]) - (b[1] - a[1]) * (p[0] - a[0]);
}

void draw_wireframe(ivec2 v0, ivec2 v1, ivec2 v2) {

  line(v0[0], v0[1], v1[0], v1[1]);
  line(v1[0], v1[1], v2[0], v2[1]);
  line(v2[0], v2[1], v0[0], v0[1]);
}

#define FAST_RASTER_CHECK(w0, w1, w2) (w0 | w1 | w2) > 0
#define SLOW_RASTER_CHECK(w0, w1, w2) (w0 >= 0) && (w1 >= 0) && (w2 >= 0)

#define RASTER_CHECK(w0, w1, w2) FAST_RASTER_CHECK(w0, w1, w2)

float interpolate_scaler(vec3 values, vec3 lambda) {
  return (values[0] * lambda[0]) + (values[1] * lambda[1]) +
         (values[2] * lambda[2]);
}

void interoplate_vec3(vec3 values[3], vec3 lambda, vec3 result) {
  for (int i = 0; i < 3; i++) {
    glm_vec3_muladds(values[i], lambda[i], result);
  }
}

void interoplate_vec2(vec2 values[3], vec3 lambda, vec2 result) {
  for (int i = 0; i < 3; i++) {
    glm_vec2_muladds(values[i], lambda[i], result);
  }
}

void calc_lambda(int w0, int w1, int w2, vec3 out) {
  float area = w0 + w1 + w2;
  out[0] = w0 / area;
  out[1] = w1 / area;
  out[2] = w2 / area;
}

void calc_persp_lambda(int w0, int w1, int w2, vec4 verts[3], vec3 out) {
  float w0d = w0 / (verts[0][3] / verts[0][2]);
  float w1d = w1 / (verts[1][3] / verts[1][2]);
  float w2d = w2 / (verts[2][3] / verts[2][2]);
  float area = w0d + w1d + w2d;
  out[0] = w0d / area;
  out[1] = w1d / area;
  out[2] = w2d / area;
}

void raster(Triangle t, bool apply_lighting) {
  ivec2 v0, v1, v2;
  clip_to_screen(t.verts[0], v0);
  clip_to_screen(t.verts[1], v1);
  clip_to_screen(t.verts[2], v2);

  vec3 depths = {t.verts[0][2], t.verts[1][2], t.verts[2][2]};

  ivec2 min, max;
  min[0] = imax(imin3(v0[0], v1[0], v2[0]), 0);
  min[1] = imax(imin3(v0[1], v1[1], v2[1]), 0);

  max[0] = imin(imax3(v0[0], v1[0], v2[0]), RENDER_WIDTH);
  max[1] = imin(imax3(v0[1], v1[1], v2[1]), RENDER_HEIGHT);
  for (i32 y = min[1]; y < max[1]; y++) {
    for (i32 x = min[0]; x < max[0]; x++) {
      ivec2 p = {x, y};
      int iw0 = edge_func(v1, v2, p);
      int iw1 = edge_func(v2, v0, p);
      int iw2 = edge_func(v0, v1, p);

      if (RASTER_CHECK(iw0, iw1, iw2)) {
        vec3 lambda;
        vec3 persp_lambda;
        calc_lambda(iw0, iw1, iw2, lambda);
        calc_persp_lambda(iw0, iw1, iw2, t.verts, persp_lambda);
        float depth = interpolate_scaler(depths, lambda);

        if (does_depth_pass(depth, x, y)) {
          write_depth(depth, x, y);
          vec3 normal = GLM_VEC3_ZERO_INIT;
          interoplate_vec3(t.norms, lambda, normal);
          glm_normalize(normal);
          u8 color = 1;

          if (apply_lighting) {
            float light_intensity = glm_vec3_dot(normal, sun_direction);
            if (light_intensity < 0.5f) {
              color = 2;
            }
          }

          draw_pixel(x, y, color);
        }
      }
    }
  }
}

void draw_mesh(Mesh *mesh, RenderMatricies *render_data, bool apply_lighting) {
  glm_normalize(sun_direction);
  for (u32 i = 0; i < mesh->indicies_len; i += 3) {
    Triangle t;
    bool clip_triangle = false;
    for (u32 vertex = 0; vertex < 3; vertex++) {
      MeshIndex index = mesh->indicies[i + vertex];

      // vertex
      vec4 model;
      glm_vec4(mesh->verticies[get_vertex_index(index)], 1.0f, model);
      vec4 clip;
      glm_mat4_mulv(render_data->mvp, model, clip);
      clip[0] = clip[0] / clip[3];
      clip[1] = clip[1] / clip[3];
      clip[2] = clip[2] / clip[3];
      clip[3] = 1 / clip[3];
      // tracef("fragcord.w: %f", clip[3]);
      glm_vec4_copy(clip, t.verts[vertex]);

      if (clip[2] < 0 || clip[2] > 1) {
        clip_triangle = true;
        break;
      }

      // normal
      vec3 norm;
      glm_vec3_copy(mesh->normals[get_normal_index(index)], norm);
      glm_mat3_mulv(render_data->norm, norm, norm);
      glm_vec3_normalize(norm);
      glm_vec3_copy(norm, t.norms[vertex]);

      // uv
      vec2 uv;
      glm_vec2_copy(mesh->uvs[get_uv_index(index)], uv);
      glm_vec2_copy(uv, t.uvs[vertex]);
    }

    if (!clip_triangle) {
      raster(t, apply_lighting);
    }
  }
}

void render_clear_depth_buffer() {
  memset(depth_buffer, DEPTH_CLEAR_VALUE,
         sizeof(udepth) * RENDER_WIDTH * RENDER_HEIGHT);
}
uint32_t compute_gray_q(uint8_t v) {
  uint32_t channel = v & 0xFF;
  return (uint32_t)((channel << 16) | (channel << 8) | (channel));
}

uint32_t compute_gray(float v) {
  if (v < 0) {
    return 0;
  } else if (v >= 1) {
    return 0xFFFFFF;
  }

  uint8_t q = (uint8_t)(v * 255);
  return compute_gray_q(q);
}
void draw_depth_buffer() {
  PALETTE[0] = compute_gray(0);
  PALETTE[1] = compute_gray(0.33f);

  PALETTE[2] = compute_gray(0.66f);
  PALETTE[3] = compute_gray(1.0f);

  for (i32 x = 0; x < RENDER_WIDTH; x++) {
    for (i32 y = 0; y < RENDER_HEIGHT; y++) {
      udepth d = depth_buffer[y * RENDER_WIDTH + x];
      u8 color = 0;
      if (d > 64) {
        color = 1;
      }
      if (d > 128) {
        color = 2;
      }
      if (d > 244) {
        color = 3;
      }
      draw_pixel(x, y, color);
    }
  }
}

static const i32 CROSS_X[4] = {1, 0, 0, -1};

static const i32 CROSS_Y[4] = {0, 1, -1, 0};

i32 depth_convolve_2x2(i32 x, i32 y, const i32 *kernel) {
  return get_depth(x, y) * kernel[0] + get_depth(x + 1, y) * kernel[1] +
         get_depth(x, y + 1) * kernel[2] + get_depth(x + 1, y + 1) * kernel[3];
}

void draw_outlines() {

  for (i32 y = 1; y < RENDER_HEIGHT - 1; y++) {
    for (i32 x = 1; x < RENDER_WIDTH - 1; x++) {
      i32 cross_x = depth_convolve_2x2(x, y, CROSS_X);
      i32 cross_y = depth_convolve_2x2(x, y, CROSS_Y);

      i32 grad = abs(cross_x) + abs(cross_y);

      if (grad > 30) {
        draw_pixel(x, y, 0);
      }
    }
  }
}
