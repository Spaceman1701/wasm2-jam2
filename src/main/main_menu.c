#include "main_menu.h"
#include "wasm4.h"

#include "cglm/affine.h"
#include "cglm/quat.h"

#include "cglm/cam.h"
#include "render.h"
#include "resources.h"

void init_menu_world(MainMenuWorld* w) {
  w->gamepad.gamepad = GAMEPAD1;
  w->finished = false;

  transform_init(&w->camera.transform);

  glm_perspective(glm_rad(60), 1, 1, 100, w->camera.projection);
  glm_mat4_copy(GLM_MAT4_IDENTITY, w->camera.view);

  w->ship_mesh = mesh_smallship_new();

  w->frame = 0;
  glm_vec3_copy((vec3){0, -1, 1}, sun_direction);
  w->launching_game = false;
}

void step_menu_world(MainMenuWorld* w) {
  render_clear_depth_buffer();
  sys_record_inputs(&w->gamepad);

  *DRAW_COLORS = 4;
  text("*STAR PILOT*", 80 - (11 * 8) / 2, 10);
  text("Press Any Button", 80 - (16 * 8) / 2, 150);

  if (w->gamepad.any_button_down) {
    w->finished = true;
  }
  mat4 model = GLM_MAT4_IDENTITY;
  glm_translate(model, (vec3){0, 0, -22});

  float frame_addon = w->frame * 0.3f;
  glm_rotate(model, glm_rad(75 + frame_addon), GLM_YUP);
  glm_rotate(model, glm_rad(-40), GLM_ZUP);
  glm_rotate(model, glm_rad(-50), GLM_XUP);
  w->frame += 1;

  RenderMatricies m;
  compute_render_matricies(model, w->camera.view, w->camera.projection, &m);
  draw_mesh(&w->ship_mesh, &m, true);
}
