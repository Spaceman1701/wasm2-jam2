#include "gamepad.h"
#include "wasm4.h"
#include <stdbool.h>

void update_button(Button *b, bool poll) {
  if (b->down && !poll) { // just released
    b->released = true;
    b->down = false;
    b->pressed = false;
    b->frames_since_release = 0;

    return;
  }
  b->frames_since_release += 1; // not released this frame, update counter
  if (!b->down && poll) {       // just pressed
    b->down = true;
    b->pressed = true;
    return;
  }

  // either remained pressed or remained released
  if (b->released) {
    b->released = false;
  }
  if (b->pressed) {
    b->pressed = false;
  }
}

void sys_record_inputs(Gamepad *gamepad) {
  u8 state = *gamepad->gamepad;

  update_button(&gamepad->up, state & BUTTON_UP);
  update_button(&gamepad->right, state & BUTTON_RIGHT);
  update_button(&gamepad->down, state & BUTTON_DOWN);
  update_button(&gamepad->left, state & BUTTON_LEFT);
  update_button(&gamepad->button_one, state & BUTTON_1);
  update_button(&gamepad->button_two, state & BUTTON_2);

  gamepad->any_button_down = state != 0;
}
