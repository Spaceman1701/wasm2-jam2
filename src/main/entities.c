#include "entities.h"
#include "cglm/quat.h"
#include "cglm/vec3.h"
#include "particle.h"
#include "transform.h"
#include "utils.h"
#include "wasm4.h"
#include <math.h>
void init_torpedo(Torpedo *t, vec3 starting_position, vec3 velocity) {
  transform_init(&t->transform);
  glm_vec3_copy(velocity, t->velocity);
  glm_vec3_mul(velocity, (vec3){2, 2, 2}, velocity);
  glm_vec3_copy(starting_position, t->transform.pos);

  vec3 dir;
  glm_normalize_to(velocity, dir);

  vec3 rotation_axis;
  glm_vec3_cross(dir, GLM_YUP, rotation_axis);
  float cos_angle = glm_vec3_dot(dir, rotation_axis);
  float rads = acosf(cos_angle);

  glm_quatv(t->transform.rot, rads, rotation_axis);

  t->ttl = 60 * 10;
}
void update_torpedo(Torpedo *t) {
  transform_translate(&t->transform, t->velocity);
  t->ttl -= 1;
}

#define ASTEROID_SCALE                                                         \
  (vec3) { 8, 8, 8 }

void init_asteroid(Asteroid *a, vec3 pos, vec3 rotation_axis,
                   float rotation_velocity) {
  transform_init(&a->transform);
  glm_vec3_copy(pos, a->transform.pos);

  a->rotation_velocity = rotation_velocity;
  glm_vec3_copy(rotation_axis, a->rotation_axis);

  glm_vec3_copy(ASTEROID_SCALE, a->transform.scale);
  versor rotation;
  glm_quatv(rotation, frand(), a->rotation_axis);

  transform_rotate(&a->transform, rotation);
}
void update_asteroid(Asteroid *a) {
  versor rotation;
  glm_quatv(rotation, a->rotation_velocity, a->rotation_axis);
  transform_rotate(&a->transform, rotation);
}

void init_explosion(Explosion *e, vec3 start_pos, vec3 velocity, i32 ttl) {
  for (i32 i = 0; i < 20; i++) {
    glm_vec3_copy(start_pos, e->particle_pos[i]);

    vec3 random_direction;
    vec3_rand(random_direction);
    for (int a = 0; a < 3; a++) {
      random_direction[a] -= 0.5f;
    }

    glm_normalize(random_direction);
    glm_vec3_scale(random_direction, 1.1f, random_direction);

    glm_vec3_add(random_direction, velocity, e->particle_vel[i]);
  }

  e->ttl = ttl;
}

void step_explosion(Explosion *e) {
  PaticleCollection pc = {
      .count = 20,
      .pos = e->particle_pos,
      .vel = e->particle_vel,
  };

  update_particles(pc);
  e->ttl -= 1;
}

void draw_explosion(Explosion *e, mat4 view, mat4 proj) {
  PaticleCollection pc = {
      .count = 20,
      .pos = e->particle_pos,
      .vel = e->particle_vel,
  };

  draw_paticles(pc, 1, view, proj);
}

void init_fighter(Fighter *f, vec3 pos, float vel, vec3 player_pos) {
  transform_init(&f->transform);
  glm_vec3_copy(pos, f->transform.pos);
  f->health = 100;

  glm_vec3_add(player_pos, (vec3){0, 0, -150}, f->waypoint);

  float turn_dir_rand = frand();
  if (turn_dir_rand > 0.5f) {
    glm_vec3_copy((vec3){1, 0, 0}, f->final_heading);
  } else {
    glm_vec3_copy((vec3){-1, 0, 0}, f->final_heading);
  }

  vec3 heading;
  glm_vec3_sub(f->waypoint, f->transform.pos, heading);
  glm_vec3_normalize(heading);

  glm_quat_from_vecs((vec3){0, 0, -1}, heading, f->transform.rot);

  f->attack_speed = vel;
  f->roll = 0;
}

void step_fighter(Fighter *f) {

  float dist = glm_vec3_distance(f->transform.pos, f->waypoint);

  vec3 final_dir;
  float turn_threshhold = f->attack_speed + 10.f;

  vec3 forward;
  transform_get_forward_vec(&f->transform, forward);

  if (dist < turn_threshhold && f->turn_frames < 120) {
    vec3 target_forward;
    glm_vec3_lerp(forward, f->final_heading, f->turn_frames / 120,
                  target_forward);

    versor rot;
    glm_quat_from_vecs(forward, target_forward, rot);

    versor roll;
    float cur_roll = glm_lerp(0, 30, f->turn_frames / 120);
    glm_quatv(roll, glm_rad(cur_roll), target_forward);

    transform_rotate(&f->transform, rot);

    transform_rotate(&f->transform, roll);
    f->turn_frames += 1;
  }

  transform_get_forward_vec(&f->transform, forward);
  glm_vec3_negate(forward);
  glm_vec3_scale(forward, f->attack_speed, forward);

  transform_translate(&f->transform, forward);
}

#define GATE_SCALE                                                             \
  (vec3) { 6, 6, 6 }

void init_gate(Gate *g, vec3 pos, vec3 rotation_axis, float rotation_speed) {
  transform_init(&g->transform);
  glm_vec3_copy(pos, g->transform.pos);
  glm_vec3_copy(rotation_axis, g->rotation_axis);
  g->rotation_speed = rotation_speed;

  versor rotation;
  glm_quatv(rotation, glm_rad(90), GLM_YUP);
  glm_quat_copy(rotation, g->transform.rot);

  glm_vec3_copy(GATE_SCALE, g->transform.scale);
}

void step_gate(Gate *g) {
  versor rotation;
  glm_quatv(rotation, g->rotation_speed, g->rotation_axis);

  transform_rotate(&g->transform, rotation);
}
