#include "mesh.h"

const u32 INDEX_MASK = 0b1111111111;
const u32 NORMAL_OFFSET = 10;
const u32 UV_OFFSET = 20;

u16 get_vertex_index(MeshIndex index) { return (u16)(index & INDEX_MASK); }

u16 get_normal_index(MeshIndex index) {
  return (index >> NORMAL_OFFSET) & INDEX_MASK;
}
u16 get_uv_index(MeshIndex index) { return (index >> UV_OFFSET) & INDEX_MASK; }
