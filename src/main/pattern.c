#include "pattern.h"
#include "cglm/quat.h"
#include "cglm/vec3.h"
#include "wasm4.h"
void circle_pattern(vec3 out, i32 i, i32 maxi, void *state) {
  CirclePatternState *s = (CirclePatternState *)state;

  float step_angle = 360.0f / (maxi);
  float cur_angle = step_angle * i;

  versor rot;
  glm_quatv(rot, cur_angle, s->axis);

  glm_quat_rotatev(rot, s->starting_position, out);
}

void cross_pattern(vec3 out, i32 i, i32 maxi, void *state) {
  CrossPatternState *s = (CrossPatternState *)state;
  maxi; // to make clang stop complaining
  if (i == 0) {
    glm_vec3_dup(s->starting_position, out);
    return;
  }

  // not necessarily named right, these are just the two
  // perpendicular axis

  vec3 axis;
  float ipos = ((float)i / 4 + 1) * s->spacing;
  if (i % 2 == 0) {
    axis[0] = s->axis[1];
    axis[1] = s->axis[2];
    axis[2] = s->axis[0];
    if (i % 4 == 0) {
      ipos *= -1;
    }
  } else {
    axis[0] = s->axis[2];
    axis[1] = s->axis[0];
    axis[2] = s->axis[1];
    if (i % 3 == 0) {
      ipos *= -1;
    }
  }
  glm_vec3_scale(axis, ipos, axis);
  glm_vec3_add(s->starting_position, axis, out);
}
