#include "world.h"
#include "cglm/mat4.h"
#include "cglm/quat.h"
#include "cglm/util.h"
#include "cglm/vec3.h"
#include "cglm/vec4.h"
#include "collision.h"
#include "entities.h"
#include "gamepad.h"
#include "gui.h"
#include "mesh.h"
#include "particle.h"
#include "pattern.h"
#include "player.h"
#include "render.h"
#include "sound.h"
#include "transform.h"
#include "utils.h"
#include "wasm4.h"

#include <stdlib.h>

#include "cglm/affine.h"
#include "cglm/cam.h"

#include "resources.h"
#include <math.h>
#include <stdbool.h>
#include <stddef.h>

#define SPEED_SPEC_COUNT 20

static vec3 speed_specs_pos[SPEED_SPEC_COUNT];
static vec3 speed_sepcs_vel[SPEED_SPEC_COUNT];

static Mesh torpedo_mesh;
static Mesh asteroid_mesh;
static Mesh gate_mesh;
static Mesh collision_mesh_proxy;
static CollisionMesh ship_collision;
static CollisionMesh asteroid_collision;
static CollisionMesh torpedo_collision;
static CollisionMesh gate_collision;

void init_torpedo_table(TorpedoTable* t) {
  t->count = 0;
  t->next_id = 0;

  for (i32 i = 0; i < MAX_TORPEDO_COUNT; i++) {
    t->index[i].id = -1;
  }
}

bool is_segment_finished(GameplayWorld* w) {
  float player_z = w->player.transform.pos[2];

  return player_z < w->segment.finish_z;
}

i32 world_spawn_asteroid(GameplayWorld* w, vec3 pos) {
  AsteroidTable* astroids = &w->asteroid_table;

  EntityTableRef ref = MAKE_TABLE_REF(astroids, MAX_ASTEROID_COUNT);

  i32 idx = table_find_spawn_slot(ref);
  if (idx < 0) {
    return -1;
  }

  EntityRef* eref = &astroids->index[idx];
  Asteroid* new = &astroids->data[idx];

  vec3 raxis;
  vec3_rand(raxis);
  glm_normalize(raxis);
  init_asteroid(new, pos, raxis, glm_rad(0.5f));

  return eref->id;
}

void random_astroid_pos(vec3 pos) {
  pos[0] = (frand() - 0.5f) * 175;
  pos[1] = (frand() - 0.5f) * 60;
  pos[2] = -150 - (frand() * 50);
}

void spawn_random_asteroid(GameplayWorld* w) {
  vec3 pos;
  random_astroid_pos(pos);

  world_spawn_asteroid(w, pos);
}

void spawn_random_asteroids(GameplayWorld* w) {
  float cam_z = w->camera.transform.pos[2];
  CirclePatternState state;
  glm_vec3_copy(GLM_ZUP, state.axis);
  glm_vec3_copy((vec3){20, 0, cam_z - 150}, state.starting_position);

  CrossPatternState cstate;
  glm_vec3_copy(GLM_ZUP, cstate.axis);
  glm_vec3_copy((vec3){0, 0, cam_z - 150}, cstate.starting_position);
  cstate.spacing = 15;
  bool circle = frand() > 0.5f;
  u8 asteroids_to_spawn = MAX_ASTEROID_COUNT / 2;
  for (u8 i = 0; i < asteroids_to_spawn; i++) {
    vec3 pos;
    if (circle) {
      circle_pattern(pos, i, asteroids_to_spawn, &state);
    } else {
      cross_pattern(pos, i, asteroids_to_spawn, &cstate);
    }
    pos[2] = cam_z - 150 - (frand() * 50);
    world_spawn_asteroid(w, pos);
  }
}

void world_init(GameplayWorld* w) {
  transform_init(&w->camera.transform);
  player_init(&w->player);

  w->speed_specs = (PaticleCollection){
      .count = SPEED_SPEC_COUNT,
      .pos = speed_specs_pos,
      .vel = speed_sepcs_vel,
  };

  set_particle_velocity(w->speed_specs, (vec3){0, 0, .25});

  glm_perspective(glm_rad(NORMAL_FOV), 1, 10.f, 200.f, w->camera.projection);

  torpedo_mesh = mesh_torpedo_new();
  asteroid_mesh = mesh_cube_new();
  collision_mesh_proxy =
      mesh_collision_proxy_new(); // should be a sphere, but there's not
                                  // enough memory
  gate_mesh = mesh_gate_new();

  ship_collision = mesh_smallship_new_collision();
  torpedo_collision = mesh_torpedo_new_collision();
  asteroid_collision = mesh_cube_new_collision();
  gate_collision = mesh_gate_new_collision();

  init_torpedo_table(&w->torpedo_table);
  // spawn_random_asteroids(w);

  w->debug_mode = DebugNone;

  w->gamepad.gamepad = GAMEPAD1;
  w->debug_gamepad.gamepad = GAMEPAD2;
  w->score = 0;

  w->camera.fov = NORMAL_FOV;
  w->camera.target_fov = NORMAL_FOV;

  randomize_segment(&w->segment);
  w->ready_for_next_segment = false;

  w->gp_state.x = 0;
  w->gp_state.y = 0;
  w->gp_state.z = -100;

  w->player_hit_frame = 1000;
  w->player.normal_speed = 1.0f;

  w->segment_number = 1;
}

void world_add_score(GameplayWorld* w, i32 base_score) {
  i32 final_score = w->player.speed * base_score;
  w->score += final_score;
}

i32 world_spawn_torpedo(GameplayWorld* w, vec3 pos, vec3 velocity, u32 ttl) {
  TorpedoTable* torpedos = &w->torpedo_table;

  EntityTableRef ref = MAKE_TABLE_REF(torpedos, MAX_TORPEDO_COUNT);

  i32 idx = table_find_spawn_slot(ref);
  if (idx < 0) {
    return -1;
  }

  EntityRef* eref = &torpedos->index[idx];
  Torpedo* new = &torpedos->data[idx];
  init_torpedo(new, pos, velocity);
  new->ttl = ttl;
  return eref->id;
}

bool world_delete_torpedo(GameplayWorld* w, i32 torpedo_id) {
  TorpedoTable* torpedos = &w->torpedo_table;

  DELETE_FUNC_BODY(torpedos, torpedo_id, Torpedo);
}

bool world_delete_asteroid(GameplayWorld* w, i32 id) {
  AsteroidTable* asteroids = &w->asteroid_table;

  DELETE_FUNC_BODY(asteroids, id, Asteroid);
}

bool world_delete_explosion(GameplayWorld* w, i32 id) {
  ExplosionTable* explosions = &w->explosion_table;
  DELETE_FUNC_BODY(explosions, id, Explosion);
}

i32 world_spawn_explosion(GameplayWorld* w, vec3 pos, vec3 vel) {
  ExplosionTable* explosions = &w->explosion_table;

  EntityTableRef ref = MAKE_TABLE_REF(explosions, MAX_EXPLOSION_COUNT);
  i32 idx = table_find_spawn_slot(ref);
  if (idx < 0) {
    return -1;
  }

  EntityRef* eref = &explosions->index[idx];
  Explosion* new = &explosions->data[idx];
  init_explosion(new, pos, vel, 60);
  return eref->id;
}

i32 world_spawn_gate(GameplayWorld* w, vec3 pos, vec3 axis, float speed) {
  GateTable* gates = &w->gate_table;

  EntityTableRef ref = MAKE_TABLE_REF(gates, MAX_GATE_COUNT);
  i32 idx = table_find_spawn_slot(ref);
  if (idx < 0) {
    return -1;
  }

  EntityRef* eref = &gates->index[idx];
  Gate* new = &gates->data[idx];
  init_gate(new, pos, axis, speed);
  return eref->id;
}

TABLE_DELETE_FUNC(Gate, world_delete_gate, &w->gate_table);

void world_draw_mesh(Transform* t, Mesh* m, GameplayWorld* w,
                     bool apply_lighting) {
  mat4 model = GLM_MAT4_IDENTITY;
  transform_to_mat4(t, model);

  RenderMatricies render_ctx;
  compute_render_matricies(model, w->camera.view, w->camera.projection,
                           &render_ctx);

  draw_mesh(m, &render_ctx, apply_lighting);
}

i32 world_spawn_fighter(GameplayWorld* w, vec3 pos, float speed,
                        vec3 player_pos) {

  ENTITY_TABLE_TYPENAME(Fighter)* fighters = &w->fighter_table;

  EntityTableRef ref = MAKE_TABLE_REF(fighters, MAX_EXPLOSION_COUNT);
  i32 idx = table_find_spawn_slot(ref);
  if (idx < 0) {
    return -1;
  }

  EntityRef* eref = &fighters->index[idx];
  Fighter* new = &fighters->data[idx];
  init_fighter(new, pos, speed, player_pos);
  return eref->id;
}

/* bool world_delete_fighter(GameplayWorld *w, i32 id) { */
/*   ENTITY_TABLE_TYPENAME(Fighter) *fighters = &w->fighter_table; */
/*   DELETE_FUNC_BODY(fighters, id, Fighter); */
/* } */

TABLE_DELETE_FUNC(Fighter, world_delete_fighter, &w->fighter_table);

void system_fire_torpedo(GameplayWorld* w) {
  if (w->gamepad.button_one.down && w->player.torpedo_cooldown == 0) {
    vec3 player_forwad_vec;
    transform_get_forward_vec(&w->player.transform, player_forwad_vec);
    glm_vec3_negate(player_forwad_vec); // TODO: player  mesh is backwards so
    // this is reversed
    glm_vec3_mul(player_forwad_vec, (vec3){6, 6, 6}, player_forwad_vec);

    i32 idx = world_spawn_torpedo(w, w->player.transform.pos, player_forwad_vec,
                                  60 * 10);

    if (idx >= 0) {
      w->player.torpedo_cooldown = 10;
      play_tone(torpedo_launch_sound);
    }
  }
}

void system_make_camera_follow(GameplayWorld* w) {
  vec3* player_pos = &w->player.transform.pos;

  vec3 new_camera_pos;
  new_camera_pos[0] = 0;
  new_camera_pos[1] = 0;
  new_camera_pos[2] = (*player_pos)[2] + 25;

  glm_vec3_copy(new_camera_pos, w->camera.transform.pos);

  vec3 player_dir;
  // glm_vec3_lerp(new_camera_pos, *player_pos, 1.f, focus_point);

  glm_vec3_sub(*player_pos, new_camera_pos, player_dir);
  glm_normalize(player_dir);

  glm_vec3_lerp((vec3){0, 0, -1}, player_dir, 0.4, player_dir);

  glm_quat_from_vecs((vec3){0, 0, -1}, player_dir, w->camera.transform.rot);

  // glm_vec3_sub(*player_pos, PLAYER_IDLE_LOCATION_INIT,
  // w->camera.transform.pos);
}

void system_spawn_fighter_on_attack(GameplayWorld* w) {
  vec3 attacks[4] = {{0, 1, 1}, {1, 0, 1}, {0, -1, 1}, {-1, 0, 1}};
  float cam_z = w->player.transform.pos[2];
  vec3 player_dir;
  transform_get_forward_vec(&w->player.transform, player_dir);
  i32 attack_index = frand() * 4;

  world_spawn_fighter(w, (vec3){0, 0, cam_z - 250}, 1.f,
                      w->player.transform.pos);
}

void system_step_fighters(GameplayWorld* w) {
  for (i32 i = 0; i < w->fighter_table.count; i++) {
    step_fighter(&w->fighter_table.data[i]);
    if (w->fighter_table.data[i].health <= 0) {
      world_delete_fighter(w, w->fighter_table.index[i].id);
      i -= 1;
    }
  }
}

void system_draw_fighters(GameplayWorld* w) {
  for (i32 i = 0; i < w->fighter_table.count; i++) {
    vec3 shape[3] = {{10, 0, 0}, {0, 0, 0}, {-10, 0, 0}};
    for (i32 j = 0; j < 3; j++) {
      Transform t = w->fighter_table.data[i].transform;
      transform_translate(&t, shape[j]);

      world_draw_mesh(&t, &w->player.mesh, w, true);
    }
  }
}

void system_fighter_torpedo_interaction(GameplayWorld* w) {
  for (i32 f = 0; f < w->fighter_table.count; f++) {
    for (i32 t = 0; t < w->torpedo_table.count; t++) {
      bool collision = collision_meshs_intersect(
          &ship_collision, &w->fighter_table.data[f].transform,
          &torpedo_collision, &w->torpedo_table.data[t].transform);
      if (collision) {
        world_delete_torpedo(w, w->torpedo_table.index[t].id);
        w->fighter_table.data[f].health -= 50;
      }
    }
  }
}

void system_update_speed_specs(GameplayWorld* w) {
  float player_speed = w->player.speed;
  PaticleCollection pc = w->speed_specs;
  vec3 player_forwad_vec;
  transform_get_forward_vec(&w->player.transform, player_forwad_vec);
  glm_vec3_mul(player_forwad_vec,
               (vec3){player_speed / 4, player_speed / 4, player_speed / 4},
               player_forwad_vec);
  update_speed_particles(pc);
  update_particles(pc);
  set_particle_velocity(pc, player_forwad_vec);
}

void system_update_view_matrix(GameplayWorld* w) {
  glm_mat4_identity(w->camera.view);
  transform_to_mat4(&w->camera.transform, w->camera.view);
  glm_mat4_inv(w->camera.view, w->camera.view);
}

void system_update_torpedos(GameplayWorld* w) {
  for (u8 i = 0; i < w->torpedo_table.count; i++) {
    Torpedo* t = &w->torpedo_table.data[i];

    update_torpedo(t);
    if (t->ttl <= 0) {
      i32 id = w->torpedo_table.index[i].id;
      world_delete_torpedo(w, id);
    }
  }
}

void system_step_explosions(GameplayWorld* w) {
  for (i32 i = 0; i < w->explosion_table.count; i++) {
    step_explosion(&w->explosion_table.data[i]);
    if (w->explosion_table.data[i].ttl < 0) {
      world_delete_explosion(w, w->explosion_table.index[i].id);
    }
  }
}

void system_draw_explosions(GameplayWorld* w) {
  for (i32 i = 0; i < w->explosion_table.count; i++) {
    draw_explosion(&w->explosion_table.data[i], w->camera.view,
                   w->camera.projection);
  }
}

void world_draw_collision_mesh(GameplayWorld* w, CollisionMesh* m,
                               Transform* t) {
  mat4 model = GLM_MAT4_IDENTITY;
  transform_to_mat4(t, model);
  for (i32 i = 0; i < m->count; i++) {
    mat4 mesh_mat;
    glm_translate_to(model, m->center[i], mesh_mat);
    vec3 scale = {m->radius[i], m->radius[i], m->radius[i]};
    glm_scale(mesh_mat, scale);

    RenderMatricies render_ctx;
    compute_render_matricies(mesh_mat, w->camera.view, w->camera.projection,
                             &render_ctx);
    draw_mesh(&collision_mesh_proxy, &render_ctx, false);
  }
}
void system_update_asteroids(GameplayWorld* w) {
  i32 distance_met = 0;
  for (u8 i = 0; i < w->asteroid_table.count; i++) {
    Asteroid* a = &w->asteroid_table.data[i];
    update_asteroid(a);

    float camera_z = w->camera.transform.pos[2];
    float az = a->transform.pos[2];

    if (az - camera_z >= 0) {
      i32 id = w->asteroid_table.index[i].id;
      world_delete_asteroid(w, id);
    }

    if (camera_z - az > -75) {
      distance_met += 1;
    }
  }
  if ((distance_met >= MAX_ASTEROID_COUNT / 2 &&
       w->asteroid_table.count <= MAX_ASTEROID_COUNT / 2) ||
      w->asteroid_table.count <= MAX_ASTEROID_COUNT / 2) {
    if (!is_segment_finished(w) && w->segment.kind == SegmentKindAsteroid) {
      spawn_random_asteroids(w);
    }
  }
  if (w->asteroid_table.count <= 0 && is_segment_finished(w) &&
      w->segment.kind == SegmentKindAsteroid) {
    w->ready_for_next_segment = true;
  } else if (w->asteroid_table.count <= 0 &&
             w->segment.kind == SegmentKindAsteroid &&
             !is_segment_finished(w)) {

    spawn_random_asteroids(w);
  }
}

void system_gate_path(GameplayWorld* w) {
  if (w->segment.kind == SegmentKindGatePath) {
    if (!is_segment_finished(w)) {
      i32 spawn_count = MAX_GATE_COUNT - w->gate_table.count;
      for (i32 i = 0; i < spawn_count; i++) {
        float z = w->gp_state.z - ((i * 25) + 25);
        w->gp_state.z = z;

        float x = w->gp_state.x + ((frand() - 0.5f) * 25);
        float y = w->gp_state.y + ((frand() - 0.5f) * 25);

        if (x > 25) {
          x = 25;
        }
        if (x < -25) {
          x = -25;
        }
        if (y > 25) {
          y = 25;
        }
        if (y < -25) {
          y = -25;
        }

        w->gp_state.x = x;
        w->gp_state.y = y;
        world_spawn_gate(w, (vec3){x, y, z}, GLM_XUP, 0);
      }
    } else {
      w->gp_state.z = -100;
      w->gp_state.x = 0;
      w->gp_state.y = 0;
      if (w->gate_table.count == 0) {
        w->ready_for_next_segment = true;
      }
    }
  }
}

void system_spinning_gate(GameplayWorld* w) {
  if (w->segment.kind == SegmentKindSpinningGate) {
    if (!w->spawed_spinning_gate) {
      vec3 axis;
      vec3_rand(axis);
      glm_normalize(axis);
      float speed = frand() * 2.0f;
      world_spawn_gate(w, (vec3){0, 0, -200}, axis, glm_rad(speed));
      w->spawed_spinning_gate = true;
    }
    if (w->spawed_spinning_gate && is_segment_finished(w)) {
      w->ready_for_next_segment = true;
      w->spawed_spinning_gate = false;
    }
  }
}

void system_draw_asteroids(GameplayWorld* w) {
  for (u8 i = 0; i < w->asteroid_table.count; i++) {
    Asteroid* a = &w->asteroid_table.data[i];
    if (w->debug_mode & DebugCollision) {
      world_draw_collision_mesh(w, &asteroid_collision, &a->transform);
    } else {
      world_draw_mesh(&a->transform, &asteroid_mesh, w, true);
    }
  }
}

void system_draw_torpedos(GameplayWorld* w) {
  for (u8 i = 0; i < w->torpedo_table.count; i++) {
    Torpedo* t = &w->torpedo_table.data[i];
    if (w->debug_mode & DebugCollision) {
      world_draw_collision_mesh(w, &torpedo_collision, &t->transform);

    } else {
      world_draw_mesh(&t->transform, &torpedo_mesh, w, false);
    }
  }
}

void system_draw_player(GameplayWorld* w) {
  if (w->debug_mode & DebugCollision) {
    world_draw_collision_mesh(w, &ship_collision, &w->player.transform);
  } else {
    world_draw_mesh(&w->player.transform, &w->player.mesh, w, true);
  }
}

void system_draw_speed_specs(GameplayWorld* w) {
  mat3 view_matrix_rot;
  glm_mat4_pick3(w->camera.view, view_matrix_rot);
  mat4 new_view = GLM_MAT4_IDENTITY_INIT;
  glm_mat4_ins3(view_matrix_rot, new_view);

  draw_paticles(w->speed_specs, 1, new_view, w->camera.projection);
}

void system_draw_health(GameplayWorld* w) {
  draw_status_bar(txt("HP:"), 4, 140, 25, ((float)w->player.hp) / PLAYER_MAX_HP,
                  4);
}

void system_draw_boost(GameplayWorld* w) {
  draw_status_bar(txt("B*:"), 4, 150, 25,
                  ((float)w->player.boost_charge) / PLAYER_BOOST_FRAMES, 4);
}

void system_astroid_torpedo_interaction(GameplayWorld* w) {
  for (i32 a = 0; a < w->asteroid_table.count; a++) {

    for (i32 t = 0; t < w->torpedo_table.count; t++) {

      bool collision = collision_meshs_intersect(
          &torpedo_collision, &w->torpedo_table.data[t].transform,
          &asteroid_collision, &w->asteroid_table.data[a].transform);

      if (collision) {
        vec3 asteroid_pos;
        vec3 vel = GLM_VEC3_ZERO_INIT;
        glm_vec3_copy(w->asteroid_table.data[a].transform.pos, asteroid_pos);

        world_delete_asteroid(w, w->asteroid_table.index[a].id);

        world_delete_torpedo(w, w->torpedo_table.index[t].id);

        world_spawn_explosion(w, asteroid_pos, vel);
        float dist_to_camera =
            fabs(w->camera.transform.pos[2] - asteroid_pos[2]);

        // w->score += 10;
        world_add_score(w, 10);
        play_tone(explosion_sound);
      }
    }
  }
}

void system_player_asteroid_interaction(GameplayWorld* w) {
  for (i32 a = 0; a < w->asteroid_table.count; a++) {

    bool collision = collision_meshs_intersect(
        &asteroid_collision, &w->asteroid_table.data[a].transform,
        &ship_collision, &w->player.transform);
    if (collision) {
      if (w->player.hp > 10) {
        w->player.hp -= 10;
      } else {
        w->player.hp = 0;
      }
      world_delete_asteroid(w, w->asteroid_table.index[a].id);
      w->player_hit_frame = 0;
    }
  }
}

void system_player_hit_display(GameplayWorld* w) {
  if (w->player_hit_frame < 5) {
    PALETTE[3] = 0xFF0000;

    PALETTE[0] = 0xFFFFFF;
    PALETTE[1] = 0;
    PALETTE[2] = 0;

    w->player_hit_frame += 1;
  } else if (w->player_hit_frame < 10) {
    w->player_hit_frame += 1;
  } else if (w->player_hit_frame < 15) {

    PALETTE[3] = 0xFF0000;

    PALETTE[0] = 0xFFFFFF;
    PALETTE[1] = 0;
    PALETTE[2] = 0;

    w->player_hit_frame += 1;
  }
}

void system_display_sight(GameplayWorld* w) {
  vec3 player_facing_dir;
  transform_get_forward_vec(&w->player.transform, player_facing_dir);
  glm_vec3_scale(player_facing_dir, 100, player_facing_dir);

  vec4 world_pos;
  glm_vec3_sub(w->player.transform.pos, player_facing_dir, world_pos);
  world_pos[3] = 1;

  mat4 vp_matrix;
  glm_mat4_mul(w->camera.projection, w->camera.view, vp_matrix);

  vec4 screen_pos;
  glm_mat4_mulv(vp_matrix, world_pos, screen_pos);
  glm_vec4_divs(screen_pos, screen_pos[3], screen_pos);

  ivec2 final_pos;
  clip_to_screen(screen_pos, final_pos);
  *DRAW_COLORS = 0;
  oval(final_pos[0] - 5, final_pos[1] - 5, 10, 10);
  *DRAW_COLORS = 4;
  hline(final_pos[0] - 2, final_pos[1], 5);
  vline(final_pos[0], final_pos[1] - 2, 5);
}

void system_set_debug_mode(GameplayWorld* w) {
  if (!DEBUG_GAMEPAD_ENABLE) {
    return;
  }
  if (w->debug_gamepad.button_one.pressed &&
      !(w->debug_mode & DebugCollision)) {
    w->debug_mode |= DebugCollision;
  } else if (w->debug_gamepad.button_one.pressed & BUTTON_1 &&
             (w->debug_mode & DebugCollision)) {
    w->debug_mode = w->debug_mode & ~DebugCollision;
  }

  if (w->debug_gamepad.button_two.pressed && !(w->debug_mode & DebugDepth)) {

    w->debug_mode |= DebugDepth;
  } else if (w->debug_gamepad.button_two.pressed &&
             (w->debug_mode & DebugDepth)) {
    w->debug_mode = w->debug_mode & ~DebugDepth;
  }
  if (w->debug_gamepad.down.pressed) {
    w->player.speed = 0;
  }
}

void system_draw_gate(GameplayWorld* w) {
  for (i32 i = 0; i < w->gate_table.count; i++) {
    if (w->debug_mode & DebugCollision) {
      world_draw_collision_mesh(w, &gate_collision,
                                &w->gate_table.data[i].transform);
    } else {
      world_draw_mesh(&w->gate_table.data[i].transform, &gate_mesh, w, true);
    }
  }
}

void system_gate_player_interaction(GameplayWorld* w) {
  for (i32 i = 0; i < w->gate_table.count; i++) {
    bool collision = collision_meshs_intersect(
        &ship_collision, &w->player.transform, &gate_collision,
        &w->gate_table.data[i].transform);
    if (collision) {
      if (w->player.hp > 25) {
        w->player.hp -= 25;
      } else {
        w->player.hp = 0;
      }
      w->player_hit_frame = 0;
    }
  }
}

void system_step_gates(GameplayWorld* w) {
  float cam_z = w->player.transform.pos[2]; // w->camera.transform.pos[2];
  for (i32 i = 0; i < w->gate_table.count; i++) {
    step_gate(&w->gate_table.data[i]);
    float gz = w->gate_table.data[i].transform.pos[2];
    if (gz > cam_z) {
      world_delete_gate(w, w->gate_table.index[i].id);
      // i--;
      // w->score += 10;`
      world_add_score(w, 10);
    }
  }
}

void system_adjust_fov(GameplayWorld* w) {
  bool fov_adjusted = false;
  if (fabs(w->camera.fov - w->camera.target_fov) < 0.2f) {
    w->camera.fov = w->camera.target_fov;
  } else {
    fov_adjusted = true;
    w->camera.fov = glm_lerp(w->camera.fov, w->camera.target_fov, 0.1f);
  }

  if (fov_adjusted) {
    glm_perspective(glm_rad(w->camera.fov), 1, 10.f, 200.f,
                    w->camera.projection);
  }
}

void system_boost(GameplayWorld* w) {
  if (w->gamepad.button_two.down) {
    w->player.speed = w->player.normal_speed * 0.25f;
    w->camera.target_fov = SLOW_FOV;
  }
  if (w->gamepad.button_two.released &&
      w->player.boost_charge > (PLAYER_BOOST_FRAMES / 2)) {
    w->player.boosting = true;
  } else if (w->gamepad.button_two.released) {
    w->player.boosting = false;
    w->player.speed = w->player.speed;
    w->camera.target_fov = NORMAL_FOV;
  }

  if (w->player.boosting) {
    w->player.boost_charge -= 1;
    w->player.speed = 2.0f * w->player.normal_speed;
    w->camera.target_fov = BOOST_FOV;
    if (w->player.boost_charge <= 0) {
      w->player.boosting = false;
      w->player.speed = w->player.normal_speed;
      w->camera.target_fov = NORMAL_FOV;
    }
  } else if (w->player.boost_charge < PLAYER_BOOST_FRAMES) {
    w->player.boost_charge += 1;
  }
}

void score_to_text(i32 score, char* out) {
  for (i32 i = 0; i < 5; i++) {
    u8 digit = (u8)(score % 10);
    score /= 10;

    out[5 - i - 1] = '0' + (char)digit;
  }
}

void system_draw_score(GameplayWorld* w) {
  char score_text[6];
  score_to_text(w->score, score_text);
  score_text[5] = 0;
  *DRAW_COLORS = 4;
  text(score_text, 4, 0);
}

void system_draw_segment_number(GameplayWorld* w) {
  char segment_number[3];
  segment_number[2] = 0;
  number_to_text(w->segment_number, segment_number, 2);
  text(segment_number, 160 - 4 - (2 * 8), 0);
}

void system_record_inputs(GameplayWorld* w) {
  sys_record_inputs(&w->gamepad);
  sys_record_inputs(&w->debug_gamepad);
}

void system_display_you_died_screen(GameplayWorld* w) {
  if (w->player.hp == 0) {
    w->player.transform.pos[2] = 0;
    w->player.speed = 0;
    PALETTE[3] = 0xFF0000;
    *DRAW_COLORS = 1;
    rect(0, 0, 160, 160);
    *DRAW_COLORS = 4;
    text("YOU DIED", 80 - (8 * 8) / 2, 80);

    char score[6];
    score[5] = 0;
    score_to_text(w->score, score);

    text("SCORE: ", 80 - (12 * 8) / 2, 96);
    text(score, 80 - (12 * 8) / 2 + (7 * 8), 96);

    text("press R to restart", 0, 150);
  }
}

void draw_segment_instructions(GameplayWorld* w) {

  switch (w->segment.kind) {
  case SegmentKindAsteroid:
    text("DESTROY!", 160 - 4 - (8 * 8), 150);
    break;
  case SegmentKindGatePath:
  case SegmentKindSpinningGate:
    text("AVOID!", 160 - 4 - (6 * 8), 150);
    break;
  }

  if (w->segment_change_frame < 60) {
    text("PREPARE", 80 - (7 * 8) / 2, 76);
    text("FOR NEXT STAGE", 80 - (15 * 8) / 2, 84);
    w->segment_change_frame += 1;
  }
}

void system_next_segment(GameplayWorld* w) {
  if (w->ready_for_next_segment) {
    randomize_segment(&w->segment);
    w->ready_for_next_segment = false;
    w->player.transform.pos[2] = 0;
    w->segment_change_frame = 0;
    w->player.normal_speed += 0.08f;
    w->player.speed = w->player.normal_speed;
    w->segment_number += 1;
  }
}

void world_step(GameplayWorld* w) {
  // Clear buffers and reset frame
  render_clear_depth_buffer();

  // handle raw inputs
  system_record_inputs(w);
  system_set_debug_mode(w);

  system_next_segment(w);
  // player position determines camera position
  system_boost(w);
  system_adjust_fov(w);
  player_update(&w->player, &w->gamepad);
  system_make_camera_follow(w);
  system_update_view_matrix(w);

  system_update_speed_specs(w);

  system_step_explosions(w);

  system_fire_torpedo(w);

  system_update_torpedos(w);
  system_update_asteroids(w);
  system_step_fighters(w);

  system_step_gates(w);
  system_gate_path(w);
  system_spinning_gate(w);

  system_astroid_torpedo_interaction(w);
  system_player_asteroid_interaction(w);
  system_fighter_torpedo_interaction(w);
  system_gate_player_interaction(w);

  system_player_hit_display(w);

  // drawing - NOTE: speed specs don't touch depth buffer
  system_draw_asteroids(w);
  system_draw_torpedos(w);
  system_draw_speed_specs(w);
  system_draw_explosions(w);
  system_draw_fighters(w);
  system_draw_gate(w);
  system_draw_player(w);
  draw_outlines();

  step_track(&baseline);
  step_track(&melody);

  if (w->debug_mode & DebugDepth) {
    draw_depth_buffer();
  }

  system_draw_health(w);
  system_draw_boost(w);
  system_display_sight(w);
  system_draw_score(w);
  system_draw_segment_number(w);
  draw_segment_instructions(w);
  system_display_you_died_screen(w);
}

i32 table_find_spawn_slot(EntityTableRef ref) {
  if (*ref.count >= ref.max_count) {
    return -1;
  }
  size_t idx = *ref.count;
  EntityRef* new_ref = &ref.index[idx];

  *new_ref = (EntityRef){
      .id = *ref.next_id,
  };

  *ref.count += 1;
  *ref.next_id += 1;

  return (i32)idx;
}

EntitySearchResult table_find_entity(i32 id, EntityRef* index, u8 count) {
  EntitySearchResult result = {
      .index = -1,
      .ref = NULL,
  };

  for (i32 i = 0; i < count; i++) {
    EntityRef* cur = &index[i];
    if (cur->id == id) {
      result.index = i;
      result.ref = cur;
      break;
    }
  }
  return result;
}

#define WORLD_WITH_SYSTEMS(...)                                                \
  system systems[sizeof((system[]){(system)__VA_ARGS__}) / sizeof(system)] = { \
      (system)__VA_ARGS__};

// TODO: expand this to make adding systems easier
