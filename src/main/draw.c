#include "draw.h"
#include "wasm4.h"
#include <stdint.h>

void draw_pixel(i32 x, i32 y, u8 draw_color) {
  int idx = (y * 160 + x) >> 2;
  // Calculate the bits within the byte that corresponds to our position
  int shift = (x & 0b11) << 1;
  int mask = 0b11 << shift;
  // Use the first DRAW_COLOR as the pixel color
  int palette_color = *DRAW_COLORS & 0b1111;
  int color = draw_color & 0b11;
  // Write to the framebuffer
  FRAMEBUFFER[idx] = (uint8_t)(color << shift) | (FRAMEBUFFER[idx] & ~mask);
}

u8 get_pixel(i32 x, i32 y) {
  int idx = (y * 160 + x) >> 2;
  // Calculate the bits within the byte that corresponds to our position
  int shift = (x & 0b11) << 1;
  int mask = 0b11;
  // Use the first DRAW_COLOR as the pixel color

  u8 buf = FRAMEBUFFER[idx];

  return (buf >> shift) & mask;
}