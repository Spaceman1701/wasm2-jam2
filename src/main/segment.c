#include "segment.h"
#include "utils.h"
#include "wasm4.h"
void randomize_segment(Segment *s) {
  float r = frand();
  tracef("segment: %f", r);
  if (r < 0.33f) {
    s->kind = SegmentKindAsteroid;
    s->finish_z = -frand() * ASTEROID_MAX_LENGTH;
  } else if (r < 0.66f) {
    s->kind = SegmentKindGatePath;
    s->finish_z = -frand() * GATE_PATH_MAX_LENGTH;
  } else {
    s->kind = SegmentKindSpinningGate;
    s->finish_z = -SPINNING_GATE_MAX_LENGTH;
  }
}
