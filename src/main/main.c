#include "cglm/mat4.h"
#include "cglm/quat.h"
#include "cglm/types.h"
#include "cglm/util.h"
#include "cglm/vec3.h"
#include "gui.h"
#include "mesh.h"
#include "particle.h"
#include "player.h"
#include "render.h"
#include "transform.h"
#include "wasm4.h"

#include "cglm/affine.h"
#include "cglm/cam.h"
#include "main_menu.h"
#include "resources.h"
#include "world.h"

#define SKIP_MENU false

typedef enum {
  WorldGameplay,
  WorldMainMenu,
} Worlds;

typedef struct {
  union {
    GameplayWorld gameplay;
    MainMenuWorld main_menu;
  };
  Worlds cur_world;
} WorldState;

WorldState world;

void start() {
  world.cur_world = WorldMainMenu;
  init_menu_world(&world.main_menu);
  if (SKIP_MENU) {
    world.main_menu.finished = true;
  }
}

void set_pallete() {
  PALETTE[0] = 0x0a0a0a;
  PALETTE[1] = 0x9dd4e0;
  PALETTE[2] = 0x00b1cc;
  PALETTE[3] = 0xa0e391;
}

void update() {
  set_pallete();
  switch (world.cur_world) {
  case WorldGameplay:
    world_step(&world.gameplay);
    break;
  case WorldMainMenu:
    step_menu_world(&world.main_menu);
    if (world.main_menu.finished) {
      world.cur_world = WorldGameplay; // start
      world_init(&world.gameplay);
    }
  }
}
