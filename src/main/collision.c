#include "collision.h"
#include "cglm/mat4.h"
#include "cglm/vec3.h"
#include "transform.h"
#include "utils.h"
#include "wasm4.h"

float scale_radius(float r, float s) { return r * s; }

bool collision_meshs_intersect(CollisionMesh *a, Transform *t_a,
                               CollisionMesh *b, Transform *t_b) {

  mat4 model_mat_a = GLM_MAT4_IDENTITY_INIT;
  mat4 model_mat_b = GLM_MAT4_IDENTITY_INIT;

  transform_to_mat4(t_a, model_mat_a);
  transform_to_mat4(t_b, model_mat_b);

  float scale_a = t_a->scale[0];
  float scale_b = t_b->scale[1];

  for (i32 i = 0; i < a->count; i++) {
    vec3 a_transformed_center;
    glm_mat4_mulv3(model_mat_a, a->center[i], 1.0f, a_transformed_center);
    for (i32 j = 0; j < b->count; j++) {
      vec3 b_transformed_center;
      glm_mat4_mulv3(model_mat_b, b->center[j], 1.0f, b_transformed_center);
      vec3 diff;
      glm_vec3_sub(a_transformed_center, b_transformed_center, diff);

      float dist2 = glm_vec3_norm(diff);
      float rdist = scale_radius(b->radius[j], scale_b) +
                    scale_radius(a->radius[i], scale_a);
      float rdist2 = rdist;

      if (dist2 < rdist2) {
        // tracef("center a: [%f, %f, %f] center b: [%f, %f, %f] "
        //        "dist=%f, rdist=%f (%d)",
        //        FMT_VEC3(a_transformed_center),
        //        FMT_VEC3(b_transformed_center), dist2, rdist2, dist2 <
        //        rdist2);
        return true;
      }
    }
  }
  return false;
}