#include "particle.h"
#include "cglm/affine.h"
#include "cglm/mat4.h"
#include "cglm/vec3.h"
#include "cglm/vec4.h"
#include "draw.h"
#include "render.h"
#include "screen.h"
#include "utils.h"
#include "wasm4.h"
#include <stdlib.h>

void update_particles(PaticleCollection particles) {
  for (u32 i = 0; i < particles.count; i++) {
    glm_vec3_add(particles.pos[i], particles.vel[i], particles.pos[i]);
  }
}
void draw_paticles(PaticleCollection particles, u8 color, mat4 view_mat,
                   mat4 proj_mat) {
  for (u32 i = 0; i < particles.count; i++) {
    vec3 *pos = &particles.pos[i];

    mat4 mvp;
    glm_mat4_mul(proj_mat, view_mat, mvp);

    // project
    vec4 clip;
    glm_vec4(particles.pos[i], 1, clip);
    glm_mat4_mulv(mvp, clip, clip);
    glm_vec4_divs(clip, clip[3], clip);

    ivec2 screen_pos;
    clip_to_screen(clip, screen_pos);

    if (screen_pos[0] < 0 || screen_pos[1] < 0 ||
        screen_pos[0] > RENDER_WIDTH || screen_pos[1] > RENDER_HEIGHT) {
      continue;
    }
    if (does_depth_pass(clip[2], screen_pos[0], screen_pos[1])) {
      draw_pixel(screen_pos[0], screen_pos[1], color);
    }
  }
}

void update_speed_particles(PaticleCollection c) {
  for (u32 i = 0; i < c.count; i++) {
    vec3 *pos = &c.pos[i];

    if ((*pos)[2] > 0) {
      (*pos)[0] = (frand() - 0.5) * 5;
      (*pos)[1] = (frand() - 0.5) * 5;
      (*pos)[2] = frand() * -5;
    }
  }
}

void set_particle_velocity(PaticleCollection c, vec3 vel) {
  for (u32 i = 0; i < c.count; i++) {
    glm_vec3_copy(vel, c.vel[i]);
  }
}