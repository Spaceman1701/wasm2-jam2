#include "sound.h"
#include "wasm4.h"

u32 tone_frequency(u16 start, u16 end) { return (u32)(start | (end << 16)); }
u32 tone_duration(u8 attack, u8 decay, u8 sustain, u8 release) {
  return (u32)((attack << 24) | (decay << 16) | sustain | (release << 8));
}
u32 tone_volume(u8 peak, u8 volume) { return (u32)((peak << 8) | volume); }
u32 tone_flags(u8 channel, u8 mode, u8 pan) {
  return (u32)(channel | mode | pan);
}

u32 play_tone(Tone t) {
  if (SOUND_ON) {
    u32 freq = tone_frequency(t.start_freq, t.end_freq);
    u32 dur = tone_duration(t.attack, t.decay, t.sustain, t.release);
    u32 vol = tone_volume(t.peak_volume, t.volume);
    u32 flags = tone_flags(t.channel, t.mode, t.pan);

    tone(freq, dur, vol, flags);
  }

  return t.attack + t.decay + t.sustain + t.release;
}

void step_track(Track *t) {
  if (t->octive_mult == 0) {
    t->octive_mult = 1;
  }
  if (t->cursor == t->tempo) {
    t->cursor = 0;
    play_tone(t->tones[t->next_note]);
    t->next_note += 1;
    t->next_note %= t->note_count;
  }
  t->cursor += 1;
}

Tone torpedo_launch_sound = {
    .start_freq = 440,
    .end_freq = 320,
    .decay = 16,
    .sustain = 10,
    .release = 28,
    .peak_volume = 100,
    .volume = 50,
    .channel = TONE_TRIANGLE,
    .mode = TONE_MODE3,
};

Tone explosion_sound = {
    .start_freq = 440,
    .end_freq = 320,
    .decay = 16,
    .sustain = 12,
    .release = 40,
    .peak_volume = 50,
    .volume = 36,
    .channel = TONE_NOISE,
    .mode = TONE_MODE3,
};

Track baseline = {
    .tempo = 30,
    .note_count = 5,
    .tones = {(Tone){
                  .channel = TONE_PULSE1,
                  .peak_volume = 30,
                  .volume = 50,
                  .sustain = 24,
                  .start_freq = A(1),
              },
              (Tone){
                  .channel = TONE_PULSE1,
                  .peak_volume = 30,
                  .volume = 50,
                  .sustain = 24,
                  .start_freq = B(1),
              },
              (Tone){
                  .channel = TONE_PULSE1,
                  .peak_volume = 30,
                  .volume = 50,
                  .sustain = 24,
                  .start_freq = C(1),
              },
              (Tone){
                  .channel = TONE_PULSE1,
                  .peak_volume = 30,
                  .volume = 50,
                  .sustain = 24,
                  .start_freq = A(1),
              },
              (Tone){
                  .channel = TONE_PULSE1,
                  .peak_volume = 30,
                  .volume = 50,
                  .sustain = 24,
                  .start_freq = B(1),
                  .mode = TONE_MODE4,
              }},
};

Track melody = {
    .tempo = 30,
    .note_count = 8,
    .tones =
        {
            (Tone){
                .channel = TONE_PULSE2,
                .start_freq = A(6),
                .peak_volume = 100,
                .volume = 50,
                .sustain = 25,
                .mode = TONE_MODE2,
            },
            (Tone){
                .channel = TONE_PULSE2,
                .start_freq = B(6),
                .peak_volume = 100,
                .volume = 50,
                .sustain = 25,
                .mode = TONE_MODE3,
            },
            (Tone){
                .channel = TONE_PULSE2,
                .start_freq = C(6),
                .peak_volume = 100,
                .volume = 50,
                .sustain = 25,
            },
            (Tone){
                .channel = TONE_PULSE2,
                .start_freq = A(6),
                .peak_volume = 100,
                .volume = 50,
                .sustain = 25,
                .mode = TONE_MODE2,
            },
            (Tone){
                .channel = TONE_PULSE2,
                .start_freq = B(6),
                .peak_volume = 100,
                .volume = 50,
                .sustain = 25,
                .mode = TONE_MODE3,
            },
            (Tone){
                .channel = TONE_PULSE2,
                .start_freq = C(6),
                .peak_volume = 100,
                .volume = 50,
                .sustain = 25,
            },
            (Tone){
                .channel = TONE_PULSE2,
                .start_freq = D(6),
                .peak_volume = 100,
                .volume = 50,
                .sustain = 25,
            },
            (Tone){
                .channel = TONE_PULSE2,
                .start_freq = B(6),
                .peak_volume = 100,
                .volume = 50,
                .sustain = 25,
            },
            (Tone){
                .channel = TONE_PULSE2,
                .start_freq = A(6),
                .peak_volume = 100,
                .volume = 50,
                .sustain = 25,
            },
        },
};
