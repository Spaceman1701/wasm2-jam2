#include "player.h"

#include "cglm/quat.h"
#include "cglm/types.h"
#include "cglm/util.h"
#include "cglm/vec3.h"
#include "gamepad.h"
#include "resources.h"
#include "transform.h"
#include "utils.h"
#include "wasm4.h"
#include <math.h>

void init_axis_angle(AxisAngle *aa, vec3 axis, float angle) {
  aa->angle = angle;
  glm_vec3_copy(axis, aa->axis);
}

void axis_angle_to_quat(AxisAngle *aa, versor out) {
  glm_quatv(out, aa->angle, aa->axis);
}

void zero_player_angles(PlayerController *player) {

  init_axis_angle(&player->pitch, GLM_XUP, 0);
  init_axis_angle(&player->roll, GLM_ZUP, 0);
  init_axis_angle(&player->yaw, GLM_YUP, 0);
}

void player_init(PlayerController *player) {
  player->mesh = mesh_smallship_new();
  player->torpedo_cooldown = 0;
  player->hp = PLAYER_MAX_HP;
  transform_init(&player->transform);

  zero_player_angles(player);

  player->target_pitch = 0;
  player->target_roll = 0;
  player->target_yaw = 0;

  player->speed = 1;
  player->boost_charge = PLAYER_BOOST_FRAMES;
  player->boosting = false;
}

void apply_roll(PlayerController *player, Gamepad *gamepad) {
  float horz_move = 0;
  if (gamepad->left.down) {
    player->target_roll = glm_rad(-30);
    player->target_yaw = glm_rad(15);
    horz_move = -0.5;
  } else if (gamepad->right.down) {
    player->target_roll = glm_rad(30);
    player->target_yaw = glm_rad(-15);
    horz_move = 0.5;
  } else {
    player->target_roll = 0;
    player->target_yaw = 0;
  }

  player->roll.angle = glm_lerp(player->roll.angle, player->target_roll, 0.1f);
  versor roll;
  axis_angle_to_quat(&player->roll, roll);
  glm_quat_copy(roll, player->transform.rot);

  player->yaw.angle = glm_lerp(player->yaw.angle, player->target_yaw, 0.1f);
  versor yaw;
  axis_angle_to_quat(&player->yaw, yaw);
  glm_quat_mul(yaw, player->transform.rot, player->transform.rot);

  vec3 horz = GLM_XUP;
  vec3_muls(horz, horz_move, horz);
  transform_translate(&player->transform, horz);
}

void clamp_player_position(PlayerController *player) {
  float x = player->transform.pos[0];
  float y = player->transform.pos[1];

  if (x > 15) {
    x = 15;
  } else if (x < -15) {
    x = -15;
  }

  if (y > 15) {
    y = 15;
  } else if (y < -15) {
    y = -15;
  }
}

void player_update(PlayerController *player, Gamepad *gamepad) {

  apply_roll(player, gamepad);

  if (gamepad->up.down) {
    player->target_pitch = glm_rad(35);
  } else if (gamepad->down.down) {
    player->target_pitch = glm_rad(-35);
  } else {
    player->target_pitch = 0;
  }
  player->pitch.angle =
      glm_lerp(player->pitch.angle, player->target_pitch, 0.1f);
  versor pitch;
  axis_angle_to_quat(&player->pitch, pitch);
  transform_rotate(&player->transform, pitch);

  vec3 forward;
  transform_get_forward_vec(&player->transform, forward);
  glm_vec3_negate(forward);
  vec3_muls(forward, player->speed, forward);
  transform_translate(&player->transform, forward);

  clamp_player_position(player);

  if (player->torpedo_cooldown > 0) {
    player->torpedo_cooldown -= 1;
  }
}
