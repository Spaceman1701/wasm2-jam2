#pragma once

#include "prelude.h"
void draw_pixel(i32 x, i32 y, u8 draw_color);
u8 get_pixel(i32 x, i32 y);