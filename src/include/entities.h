#pragma once

#include "mesh.h"
#include "player.h"
#include "transform.h"
#include <stdbool.h>

// TORPEDO
typedef struct {
  Transform transform;
  vec3 velocity;
  u32 ttl;
} Torpedo;

void init_torpedo(Torpedo *t, vec3 starting_position, vec3 velocity);
void update_torpedo(Torpedo *t);

// ASTEROID
typedef struct {
  Transform transform;
  vec3 rotation_axis;
  float rotation_velocity;
} Asteroid;

void init_asteroid(Asteroid *a, vec3 pos, vec3 rotation_axis,
                   float rotation_velocity);
void update_asteroid(Asteroid *a);

typedef struct {
  vec3 particle_pos[20];
  vec3 particle_vel[20];
  i32 ttl;
} Explosion;

void init_explosion(Explosion *e, vec3 start_pos, vec3 velocity, i32 ttl);
void step_explosion(Explosion *e);
void draw_explosion(Explosion *e, mat4 view, mat4 proj);

typedef struct {
  Transform transform;
  i8 health;
  float attack_speed;
  float roll;

  vec3 waypoint;
  vec3 final_heading;
  float turn_frames;
} Fighter;

void init_fighter(Fighter *f, vec3 pos, float vel, vec3 player_pos);
void step_fighter(Fighter *f);

typedef struct {
  Transform transform;
  vec3 rotation_axis;
  float rotation_speed;
} Gate;

void init_gate(Gate *g, vec3 pos, vec3 rotation_axis, float rotation_speed);
void step_gate(Gate *g);
