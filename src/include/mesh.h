#pragma once

#include "cglm/types.h"
#include "prelude.h"
#include <stddef.h>

typedef u32 MeshIndex;

typedef struct {
  vec3 *verticies;
  vec3 *normals;
  vec2 *uvs;

  MeshIndex *indicies;
  u16 indicies_len;
} Mesh;

u16 get_vertex_index(MeshIndex index);
u16 get_normal_index(MeshIndex index);
u16 get_uv_index(MeshIndex index);

/// CollisionMesh is a collection of spheres which represent the collider for
/// a single object
typedef struct {
  vec3 *center;  // sphere centers
  float *radius; // sphere radius

  u8 count; // collision sphere
} CollisionMesh;