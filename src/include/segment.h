#pragma once

#include "prelude.h"

#define ASTEROID_MAX_LENGTH 1000
#define GATE_PATH_MAX_LENGTH 500
#define SPINNING_GATE_MAX_LENGTH 250

typedef enum {
  SegmentKindAsteroid,
  SegmentKindGatePath,
  SegmentKindSpinningGate,
} SegmentKind;

typedef struct {
  float finish_z;
  i32 completion_score;
  SegmentKind kind;
} Segment;

void randomize_segment(Segment *s);
