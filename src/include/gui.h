#pragma once

#include "prelude.h"
typedef struct {
  char *str;
  int len;
} GuiText;

#define txt(s)                                                                 \
  (GuiText) { s, sizeof(s) - 1 }

void draw_status_bar(GuiText label, i32 x, i32 y, u32 bar_length,
                     float bar_frac, u8 bar_color);
void draw_bar(i32 x, i32 y, u32 bar_length, float bar_frac, u8 bar_color);