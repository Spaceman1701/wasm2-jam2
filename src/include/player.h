#pragma once

#include "gamepad.h"
#include "mesh.h"
#include "transform.h"

typedef struct {
  vec3 axis;
  float angle;
} AxisAngle;

typedef struct {
  Mesh mesh;
  Transform transform;
  AxisAngle pitch;
  AxisAngle roll;
  AxisAngle yaw;

  float target_roll;
  float target_pitch;
  float target_yaw;

  u8 torpedo_cooldown;
  u8 hp;

  float speed;
  float normal_speed;
  i32 boost_charge;
  bool boosting;
} PlayerController;

void player_init(PlayerController *player);
void player_update(PlayerController *player, Gamepad *gamepad);

#define PLAYER_IDLE_LOCATION                                                   \
  { 0, 6, -20 }

#define PLAYER_IDLE_LOCATION_INIT (vec3) PLAYER_IDLE_LOCATION

#define PLAYER_MAX_HP 100

#define PLAYER_BOOST_FRAMES 45
