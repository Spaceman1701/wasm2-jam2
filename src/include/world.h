#pragma once

#include <stdbool.h>
#include <stddef.h>

#include "cglm/types.h"
#include "entities.h"
#include "gamepad.h"
#include "mesh.h"
#include "particle.h"
#include "player.h"
#include "prelude.h"
#include "segment.h"
#include "transform.h"
#include "wasm4.h"

#define DEBUG_GAMEPAD_ENABLE 1

#define MAX_TORPEDO_COUNT 40
#define MAX_ASTEROID_COUNT 14
#define MAX_EXPLOSION_COUNT 5
#define MAX_FIGHTER_COUNT 0
#define MAX_GATE_COUNT 5

#define ENTITY_TABLE_TYPENAME(entity_type) entity_type##Table

#define ENTITY_TABLE_TYPE(entity_type, max)                                    \
  typedef struct {                                                             \
    entity_type data[max];                                                     \
    EntityRef index[max];                                                      \
    u8 count;                                                                  \
    i32 next_id;                                                               \
  } ENTITY_TABLE_TYPENAME(entity_type);

// uses ducktyping to make a table ref from a table
#define MAKE_TABLE_REF(t, max)                                                 \
  (EntityTableRef){.index = t->index,                                          \
                   .count = &t->count,                                         \
                   .next_id = &t->next_id,                                     \
                   .max_count = max};

typedef struct {
  Transform transform;
  mat4 projection;
  mat4 view;
  float fov;
  float target_fov;
} Camera;

typedef struct {
  u8 id;
  u16 generation;
} EntityId;

typedef struct {
  i32 id; // unique (enough) id for the entity. Negative value means invalid
} EntityRef;

typedef struct {
  i32 index;
  EntityRef* ref;
} EntitySearchResult;

ENTITY_TABLE_TYPE(Torpedo, MAX_TORPEDO_COUNT);
ENTITY_TABLE_TYPE(Asteroid, MAX_ASTEROID_COUNT);
ENTITY_TABLE_TYPE(Explosion, MAX_EXPLOSION_COUNT);
ENTITY_TABLE_TYPE(Fighter, MAX_FIGHTER_COUNT);
ENTITY_TABLE_TYPE(Gate, MAX_GATE_COUNT);

enum DebugMode {
  DebugNone = 0,
  DebugCollision = 1 << 0,
  DebugDepth = 1 << 1,
  DebugWireframe = 1 << 2,
};

typedef struct {
  float z;
  float x;
  float y;
} GatePathState;

typedef struct {
  TorpedoTable torpedo_table;
  AsteroidTable asteroid_table;
  ExplosionTable explosion_table;
  ENTITY_TABLE_TYPENAME(Fighter) fighter_table;
  ENTITY_TABLE_TYPENAME(Gate) gate_table;
  PlayerController player;
  PaticleCollection speed_specs;
  Camera camera;
  Gamepad gamepad;
  Gamepad debug_gamepad;
  u16 debug_mode;
  i32 score;
  Segment segment;
  bool ready_for_next_segment;
  GatePathState gp_state;
  bool spawed_spinning_gate;
  u16 player_hit_frame;
  u16 segment_change_frame;
  u16 segment_number;
} GameplayWorld;

typedef void (*System)(void* world);

typedef struct {
  void* data;
  System* systems;
  u16 system_count;
} ImplementedWorld;

void world_init(GameplayWorld* w);
i32 world_spawn_torpedo(GameplayWorld* w, vec3 pos, vec3 velocity, u32 ttl);
bool world_delete_torpedo(GameplayWorld* w, i32 id);
void world_step(GameplayWorld* w);

typedef struct {
  EntityRef* index;
  u8* count;
  i32* next_id;
  u8 max_count;
} EntityTableRef;

i32 table_find_spawn_slot(EntityTableRef ref);
EntitySearchResult table_find_entity(i32 id, EntityRef* index, u8 count);

#define DELETE_FUNC_BODY(table_ptr, remove_id, entity_type)                    \
  EntitySearchResult result =                                                  \
      table_find_entity(remove_id, table_ptr->index, table_ptr->count);        \
  if (result.index == -1) {                                                    \
    return false;                                                              \
  }                                                                            \
                                                                               \
  i32 replacement_index = (i32)table_ptr->count - 1;                           \
  if (replacement_index == result.index) {                                     \
                                                                               \
    result.ref->id = -1;                                                       \
    table_ptr->count -= 1;                                                     \
    return true;                                                               \
  }                                                                            \
                                                                               \
  entity_type replacement = table_ptr->data[table_ptr->count - 1];             \
  EntityRef* replacement_ref = &table_ptr->index[table_ptr->count - 1];        \
  result.ref->id = replacement_ref->id;                                        \
  table_ptr->data[result.index] = replacement;                                 \
  replacement_ref->id = -1;                                                    \
                                                                               \
  table_ptr->count -= 1;                                                       \
  return true;

#define TABLE_DELETE_FUNC(typename, funcname, table_accessor)                  \
  bool funcname(GameplayWorld* w, i32 id) {                                    \
    ENTITY_TABLE_TYPENAME(typename)* table = table_accessor;                   \
    DELETE_FUNC_BODY(table, id, typename);                                     \
  }

#define NORMAL_FOV 75
#define SLOW_FOV 55
#define BOOST_FOV 95
