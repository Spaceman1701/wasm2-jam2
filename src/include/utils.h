#pragma once

#include "cglm/types.h"
#include "prelude.h"

/// Random float between 0 and 1
float frand();

void vec3_muls(vec3 v, float s, vec3 dest);

// random vec3 where each component is between 0 and 1
void vec3_rand(vec3 v);
void number_to_text(i32 value, char *out, u32 len);
#define FMT_VEC3(v) v[0], v[1], v[2]
