#pragma once

#include "cglm/types.h"
#include "mesh.h"
#include "transform.h"
#include <stdbool.h>

extern vec3 sun_direction;

typedef struct {
  vec4 verts[3];
  vec3 norms[3];
  vec2 uvs[2];
} Triangle;

typedef void(render_mode)(Triangle *t);

// rasterize the CLIP SPACE triangle
void raster(Triangle t, bool apply_lighting);

// draw the given mesh using the Model View Projection Matrix
void draw_mesh(Mesh *mesh, RenderMatricies *render_data, bool apply_lighting);

// Clear the depth buffer to DEPTH_CLEAR_VALUE
void render_clear_depth_buffer();

void draw_depth_buffer();

void clip_to_screen(vec3 clip_vec, ivec2 out);

void draw_outlines();

bool does_depth_pass(float d, i32 x, i32 y);