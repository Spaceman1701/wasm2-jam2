#pragma once

#include "cglm/types.h"
#include "mesh.h"
#include "transform.h"
#include <stdbool.h>

typedef struct {
  vec3 box[2];
  struct {
    vec3 min;
    vec3 max;
  };

} AABB;

void init_aabb_from_mesh(AABB *aabb, Mesh *m);

bool collision_meshs_intersect(CollisionMesh *a, Transform *t_a,
                               CollisionMesh *b, Transform *t_b);