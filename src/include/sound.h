#pragma once
#include "prelude.h"
#include "wasm4.h"

#define SOUND_ON 1

typedef struct {
  u8 channel;
  u8 mode;
  u8 pan;

  u8 peak_volume;
  u8 volume;

  u8 attack;
  u8 decay;
  u8 sustain;
  u8 release;
  u16 start_freq;
  u16 end_freq;
} Tone;

typedef struct {
  u32 tempo;
  u32 cursor;
  u32 note_count;
  u32 next_note;
  u32 octive_mult;
  Tone tones[];
} Track;

u32 tone_frequency(u16 start, u16 end);
u32 tone_duration(u8 attack, u8 decay, u8 sustain, u8 release);
u32 tone_volume(u8 peak, u8 volume);
u32 tone_flags(u8 channel, u8 mode, u8 pan);

// plays a tone and returns total duration
u32 play_tone(Tone t);
void step_track(Track *t);

extern Tone torpedo_launch_sound;
extern Tone explosion_sound;
extern Track baseline;
extern Track melody;

#define NOTE(frequency, octive) (u16)(frequency * octive)

#define A(octive) NOTE(55.00f, octive)
#define As(octive) NOTE(58.27f, octive)
#define B(octive) NOTE(61.74f, octive)
#define C(octive) NOTE(65.41f, octive)
#define Cs(octive) NOTE(69.30f, octive)
#define D(octive) NOTE(73.42f, octive)
#define Ds(octive) NOTE(77.78f, octive)
#define E(octive) NOTE(82.41f, octive)
#define F(octive) NOTE(87.31f, octive)
#define Fs(octive) NOTE(92.50f, octive)
#define G(octive) NOTE(98.0f, octive)
#define Af(octive) NOTE(103.83, octive)
