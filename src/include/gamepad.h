#pragma once

#include "prelude.h"
#include <stdbool.h>

typedef struct {
  bool down;
  bool released;
  bool pressed;
  u32 frames_since_release; // for detecting double taps
} Button;

typedef struct {
  const u8 *gamepad;
  bool any_button_down;
  Button up;
  Button down;
  Button left;
  Button right;
  Button button_one;
  Button button_two;
} Gamepad;

void sys_record_inputs(Gamepad *gamepad);
