#pragma once

#include "prelude.h"
#include "transform.h"

typedef struct {
  vec3 *pos;
  vec3 *vel;
  u32 count;
} PaticleCollection;

void update_particles(PaticleCollection particles);
void draw_paticles(PaticleCollection particles, u8 color, mat4 view_mat,
                   mat4 proj_mat);
void update_speed_particles(PaticleCollection c);
void set_particle_velocity(PaticleCollection c, vec3 vel);