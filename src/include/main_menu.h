#pragma once

#include "gamepad.h"
#include "world.h"
#include <stdbool.h>
typedef struct {
  Mesh ship_mesh;
  Camera camera;
  Gamepad gamepad;
  bool finished;
  u32 frame;
  bool launching_game;
  u32 launch_frame;
} MainMenuWorld;

void init_menu_world(MainMenuWorld *w);
void step_menu_world(MainMenuWorld *w);
