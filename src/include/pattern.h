#pragma once

#include "prelude.h"

typedef void (*pattern3)(vec3 out, i32 i, i32 max_i, void *state);

typedef struct {
  vec3 axis;
  vec3 starting_position;
} CirclePatternState;

void circle_pattern(vec3 out, i32 i, i32 maxi, void *state);

typedef struct {
  vec3 axis;
  vec3 starting_position;
  float spacing;
} CrossPatternState;

void cross_pattern(vec3 out, i32 i, i32 maxi, void *state);
