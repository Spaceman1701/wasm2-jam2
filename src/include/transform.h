#pragma once

#include "cglm/types.h"

typedef struct {
  vec3 scale;
  vec3 pos;
  versor rot;
} Transform;

void transform_init(Transform *t);
void transform_to_mat4(Transform *t, mat4 out);
void transform_translate(Transform *t, vec3 translation);
void transform_rotate(Transform *t, versor rotation);
void transform_get_forward_vec(Transform *t, vec3 out);
void transform_get_up_vec(Transform *t, vec3 out);
void transform_get_right_vec(Transform *t, vec3 out);

typedef struct {
  mat4 mv;   // To camera space
  mat3 norm; // preserve direction with no translation
  mat4 mvp;  // to clip space
} RenderMatricies;

void compute_render_matricies(mat4 model, mat4 view, mat4 projection,
                              RenderMatricies *out);