ifndef WASI_SDK_PATH
$(error Download the WASI SDK (https://github.com/WebAssembly/wasi-sdk) and set $$WASI_SDK_PATH)
endif

CC = "$(WASI_SDK_PATH)/bin/clang" --sysroot="$(WASI_SDK_PATH)/share/wasi-sysroot"
CXX = "$(WASI_SDK_PATH)/bin/clang++" --sysroot="$(WASI_SDK_PATH)/share/wasi-sysroot"

# Optional dependency from binaryen for smaller builds
WASM_OPT = wasm-opt
WASM_OPT_FLAGS = -Oz --zero-filled-memory --strip-producers

# Whether to build for debugging instead of release
DEBUG = 0

# Compilation flags
CFLAGS = -W -Wall -Wextra -Werror -Wno-unused  -Wsign-conversion -MMD -MP -fno-exceptions
CFLAGS += -DCGLM_ALL_UNALIGNED -DCGLM_FORCE_DEPTH_ZERO_TO_ONE
CFLAGS += -DNDEBUG -Oz -flto
CFLAGS += -Icglm/include -Isrc/include -Ibuild/res


# Linker flags
LDFLAGS = -Wl,-zstack-size=14752,--no-entry,--import-memory -mexec-model=reactor \
	-Wl,--initial-memory=65536,--max-memory=65536,--stack-first

LDFLAGS += -Wl,--strip-all,--gc-sections,--lto-O3 -Oz


OBJECTS = $(patsubst src/main/%.c, build/%.o, $(wildcard src/main/*.c))
OBJECTS += $(patsubst res/%.obj, build/%.obj.o, $(wildcard res/*.obj))

DEPS = $(OBJECTS:.o=.d)

ifeq ($(OS), Windows_NT)
	MKDIR_BUILD = if not exist build md build
	RMDIR = rd /s /q
else
	MKDIR_BUILD = mkdir -p build
	RMDIR = rm -rf
endif

all: build/cart.wasm

# Link cart.wasm from all object files and run wasm-opt
build/cart.wasm: $(OBJECTS)
	$(CXX) -o $@ $(OBJECTS) $(LDFLAGS)
	$(WASM_OPT) $(WASM_OPT_FLAGS) $@ -o $@

src/main/*.c: build/%.obj.c

# Compile C sources
build/%.o: src/main/%.c
	@$(MKDIR_BUILD)
	$(CC) -c $< -o $@ $(CFLAGS)

build/%.obj.o: build/res/%.obj.c
	$(CC) -c $< -o $@ $(CFLAGS)

build/%.obj.c:
	python pack.py


.PHONY: clean
clean:
	$(RMDIR) build

-include $(DEPS)
